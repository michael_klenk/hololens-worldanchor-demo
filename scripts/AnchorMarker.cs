﻿using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorMarker : MonoBehaviour {

    [SerializeField]
    private Material AnchorAttached;

    [SerializeField]
    private Material AnchorDetached;

    public string Id { get; set; }

    private bool IsAttacheded { get; set; }

    void Start()
    {
        // Adding anchor initially
        gameObject.GetComponent<HandDraggable>().StartedDragging += () =>
        {
            DetachAnchor();

        };
        gameObject.GetComponent<HandDraggable>().StoppedDragging += () =>
        {
            AttachAnchor();
        };
        AttachAnchor();
    }



    internal void AttachAnchor()
    {
        WorldAnchorManager.Instance.AttachAnchor(gameObject, Id);
        GetComponentInChildren<Renderer>().material = AnchorAttached;
        Debug.Log("+Anchor attached for: " + this.gameObject.name + " - AnchorID: " + Id);
        IsAttacheded = true;
    }

    internal void DetachAnchor()
    {
        WorldAnchorManager.Instance.RemoveAnchor(gameObject);
        GetComponentInChildren<Renderer>().material = AnchorDetached;
        Debug.Log("-Anchor dettached for: " + this.gameObject.name + " - AnchorID: " + Id);
        IsAttacheded = false;
    }

    public override bool Equals(object obj)
    {

        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        AnchorMarker otherMarker = obj as AnchorMarker;
        return Id.Equals(otherMarker.Id);
    }

    public override int GetHashCode()
    {
        return Id.GetHashCode();
    }
}
