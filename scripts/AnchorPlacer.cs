﻿using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.XR.WSA.Persistence;

public class AnchorPlacer : MonoBehaviour, IInputClickHandler {
    private const string MARKERS_FILENAME = "markers";
    [SerializeField]
    private GameObject WorldAnchorMarker;

    private List<string> PersistedAnchorMarkersIds;
    private List<GameObject> Markers = new List<GameObject>();

    void Start()
    {
        InputManager.Instance.AddGlobalListener(this.gameObject);
        LoadAnchorMarkers();
    }

    private void LoadAnchorMarkers()
    {
        PersistedAnchorMarkersIds = ReadMarkersFromFile();
        WorldAnchorStore.GetAsync(WorldAnchorStoreLoaded);

    }

    private void WorldAnchorStoreLoaded(WorldAnchorStore store)
    {

        string[] anchorIdsInStore = store.GetAllIds();
        foreach (string persistedMarkerId in PersistedAnchorMarkersIds)
        {
            if (Array.Exists(anchorIdsInStore, id => id == persistedMarkerId))
            {
                Debug.Log("Reload Marker from store with id " + persistedMarkerId);
                GameObject markerClone = (GameObject)Instantiate(WorldAnchorMarker, Vector3.zero, Quaternion.identity);
                AnchorMarker anchorMarker = markerClone.GetComponent<AnchorMarker>();
                anchorMarker.Id = persistedMarkerId;
                anchorMarker.AttachAnchor();
            }
            else
            {
                Debug.Log("Marker with id " + persistedMarkerId + " was not found in AnchorStore");
                PersistedAnchorMarkersIds.Remove(persistedMarkerId);

            }
        }
        SaveMarkersToFile();
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        if (eventData.selectedObject != null)
        {
            GameObject selectedObject = eventData.selectedObject;
            AnchorMarker marker = null;
            try
            {
                marker = selectedObject.transform.parent.GetComponent<AnchorMarker>();
            } catch { }
            // Clicked Object is not a Marker, create a new one
            if (marker == null)
            {
                AnchorMarker newCreatedMarker = CreateNewMarker();
                PersistedAnchorMarkersIds.Add(newCreatedMarker.Id);
                Markers.Add(newCreatedMarker.gameObject);
                SaveMarkersToFile();
            }
        }
        eventData.Use();
    }

    private AnchorMarker CreateNewMarker()
    {
        Vector3 hitPoint = GazeManager.Instance.HitPosition;
        Vector3 directionToCamera = hitPoint - CameraCache.Main.transform.position;
        directionToCamera.y = 0f;
        GameObject marker = (GameObject)Instantiate(WorldAnchorMarker, hitPoint, Quaternion.LookRotation(directionToCamera));
        //marker.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        AnchorMarker anchorMarker = marker.GetComponent<AnchorMarker>();
        anchorMarker.Id = Guid.NewGuid().ToString();
        anchorMarker.AttachAnchor();
        return anchorMarker;
    }




    public List<string> ReadMarkersFromFile()
    {
        string path = string.Format("{0}/{1}.json", Application.persistentDataPath, MARKERS_FILENAME);

        if (UnityEngine.Windows.File.Exists(path)) { 
            byte[] data = UnityEngine.Windows.File.ReadAllBytes(path);
            string json = Encoding.ASCII.GetString(data);
            Debug.Log("Data Loaded from file " + path);
            return JsonConvert.DeserializeObject<List<string>>(json);
        } else {
            Debug.Log("No existing data file found, initialize empty marker list");
            return new List<string>();
        }
    }

    public void SaveMarkersToFile()
    {
        string path = string.Format("{0}/{1}.json", Application.persistentDataPath, MARKERS_FILENAME);

        string json = JsonConvert.SerializeObject(PersistedAnchorMarkersIds, Formatting.Indented);
        byte[] data = Encoding.ASCII.GetBytes(json);

        UnityEngine.Windows.File.WriteAllBytes(path, data);
        Debug.Log("Data Saved to file " + path);
    }

    public void ResetScene()
    {
        Debug.Log("Clear Scene");
        foreach(GameObject marker in Markers)
        {
            GameObject.Destroy(marker);
        }
        PersistedAnchorMarkersIds.Clear();
        SaveMarkersToFile();
    }

}
