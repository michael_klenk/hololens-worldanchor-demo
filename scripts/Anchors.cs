﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anchors {

    public List<string> anchorIds { get; set; }

    public Anchors()
    {
        anchorIds = new List<string>();
    }

    public void AddAnchor(AnchorMarker anchorMarker)
    {
        anchorIds.Add(anchorMarker.Id);
    }

    internal void RemoveAnchor(string anchorId)
    {
        anchorIds.Remove(anchorId);
    }

    internal void ClearAnchors()
    {
        anchorIds.Clear();
    }
}
